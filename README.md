## Installation
Zum Ausführen des Programms wird [Python 2.7](https://www.python.org/downloads/) benötigt. Damit kann dann die Datei python/main.py direkt ausgeführt werden.

## Bedienung

### Zahleneingabe
Mit "0"-"9" und ".". Rücktaste löscht die hinterste Ziffer, "-" negiert, Enter bestätigt die Eingabe. Wissenschaftliche Notation wird momentan nicht unterstützt.

### Simulationsparameter setzen
Enter beginnt die Auswahlmöglichkeit, der Parameter wird durch seine zugeordnete Taste ausgewählt:

| Parameter                                              | Taste |
| ------------------------------------------------------ | ----- |
|   U<sub>s</sub> (Schwellspannung der Diode [Volt])     |   s   |
|   C<sub>0</sub> (Kapazität der Diode bei 0V [Farad])   |   c   |
|   L (Induktivität [Henry])                             |   l   |
|   R (Widerstand [Ohm])                                 |   r   |
|   A (Amplitude der Anregungsspannung [Volt])           |   a   |
|   f (Frequenz der Anregungsspannung [Hertz])           |   f   |
|   tstep (Zeitschritt der Simulation [Sekunden])        |   t   |

Die Startwerte für die Simulation können kombiniert über die Leertaste geändert werden.

### Simulation fortsetzen
Die Simulation kann durch Gedrückt-Halten der linken Umschalttaste fortgesetzt werden.

### Anzeige an Inhalt anpassen
Mit "s" (für "skalieren") können zu jedem Zeitpunkt die berechneten Punkte neu skaliert werden, sodass alle Punkte sichtbar sind.
