# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from math import exp, sin, pi
import pyglet
from vec2 import vec2
from multiprocessing import Process, Queue
import Queue as queue
from copy import deepcopy
import pickle

window = pyglet.window.Window(640, 420)
window.clear()

points = []
scaled_points = []
last_t = 0

class number_entry:
    
    def __init__(self):

        self.number = 0
        self.exponent = 0
        self.decimal = False
        self.negative = False

    def keypress(self, char):

        digit = ord(char) - 48
        if 0 <= digit <= 9:
            self.number *= 10
            self.number += digit
            if self.decimal:
                self.exponent -= 1

        elif char == "-":
            if self.negative:
                self.negative = False
            else:
                self.negative = True

        elif ord(char) == 127:

            if self.decimal:
                self.exponent += 1
                if self.exponent == 1:
                    self.exponent = 0
                    self.decimal = False
                else:
                    self.number = self.number // 10

            else:
                self.number = self.number // 10

        elif char == ".":
            if not self.decimal:
                self.decimal = True

    def get_number(self):

        number = float(self.number)
        if self.decimal:
            number *= 10**self.exponent
        if self.negative:
            number *= -1
        return number

    def __str__(self):

        #return str(self.get_number())

        s = ""
        if self.negative:
            s += "-"
        if self.decimal:
            numberstr = str(self.number)
            if self.exponent == 0:
                s += numberstr + "."
            else:
                if len(numberstr) <= -self.exponent:
                    numberstr = (-self.exponent-len(numberstr)+1)*"0" + numberstr
                s += numberstr[:self.exponent] + "." + numberstr[self.exponent:]
        else:
            s += str(self.number)

        return s

def simulate(output_queue, change_parameters={}): #TODO: *args?

    parameters = deepcopy(simulation_parameters)
    parameters.update(change_parameters)
    process = Process(target=rk4calc, args=(parameters["x"], parameters["x'"], parameters["t0"], parameters["tstep"], parameters["tend"], parameters["Us"], parameters["C0"], parameters["L"], parameters["R"], parameters["A"], parameters["f"]*2*pi, output_queue))
    process.start()

def init_progressive_simulate(reset=True, change_parameters={}):

    if reset:
        del points[:]
        del scaled_points[:]
        draw_points.colors[:] = (0.0, 0.0, 0.0, 0.0)*VBLEN

    ui_state["simulating"] = True
    output_queue = Queue()
    simulate(output_queue, change_parameters)
    pyglet.clock.schedule_interval_soft(progressive_simulate, 1/30., output_queue)

def progressive_simulate(dt, output_queue):

    global last_t, last_point

    while True:
        try:
            point = output_queue.get_nowait()
            if type(point) == tuple:
                pyglet.clock.unschedule(progressive_simulate)
                ui_state["simulating"] = False
                (last_t, last_point) = point
                break #also do some other stuff

            points.append(point)#TODO: keep points, now that we have to store the last point anyway?
            scaled_points.append(vec2((point.x-scaler["xoffset"])*scaler["xscale"]+20, (point.y-scaler["yoffset"])*scaler["yscale"]+10))
            draw_points.vertices[(len(points)-1)*2:len(points)*2] = [int(scaled_points[-1].x), int(scaled_points[-1].y)]
            if len(points) > 1:
                draw_points.colors[(len(points)-2)*4:(len(points)-1)*4] = (1.0, 1.0, 1.0, 1.0)
        except queue.Empty:
            break


def rk4calc(x, xdash, t0, tstep, tend, Us, C0, L, R, A, omega, output_queue):

    t = t0
    q = vec2(x, xdash)
    h = tstep
    end_t = tend

    #output_queue.put(w)

    #f = lambda z, w: vec2(w[1], -(u*exp(w[0])+v)*w[1] - exp(w[0]) +1)
    #f = lambda z, w: vec2(w[1], -(u*exp(w[0])+v)*w[1] - s*(exp(w[0]*i) -1) + A*sin(omega*z)) #NOTE: for our case, u will probably always be zero.
    f = lambda t, q: vec2(q[1], -Us/L*(exp(q[0]/(C0*Us)) -1) - R/L*q[1] + A/L*sin(omega*t))

    while t <= end_t:

        k1 = f(t, q)
        k2 = f(t+h/2, q+k1*(h/2))
        k3 = f(t+h/2, q+k2*(h/2))
        k4 = f(t+h, q+k3*h)

        q = q + (k1 + k2*2 + k3*2 + k4)*(h/6)
        t = t + h

        output_queue.put(vec2(A*sin(omega*t), q[1]))
        #output_queue.put(q)

    output_queue.put((t, q))

def autoscale():
    pmax = vec2(points[0].x, points[0].y)
    pmin = vec2(points[0].y, points[0].y)
    for point in points:
        if point.x > pmax.x:
            pmax.x = point.x
        if point.x < pmin.x:
            pmin.x = point.x
        if point.y > pmax.y:
            pmax.y = point.y
        if point.y < pmin.y:
            pmin.y = point.y

    span = pmax - pmin
    xscale = 600/span.x
    yscale = 400/span.y

    #scaled_points[:] = [vec2((point.x-pmin.x)*xscale+20, (point.y-pmin.y)*yscale+10) for point in points]
    print "max current: "+str(pmax.y)+"A; min current: "+str(pmin.y)
    scaler["xscale"] = xscale
    scaler["yscale"] = yscale
    scaler["xoffset"] = pmin.x
    scaler["yoffset"] = pmin.y
    scaler["xmin"] = pmin.x
    scaler["ymin"] = pmin.y
    scale()


def scale():

    scaled_points[:] = [vec2((point.x-scaler["xoffset"])*scaler["xscale"]+20, (point.y-scaler["yoffset"])*scaler["yscale"]+10) for point in points]

def read_scaled_points():
    scaled_zipped_points = []
    for point in scaled_points:
        scaled_zipped_points.extend([int(point.x), int(point.y)])
    draw_points.vertices[:len(scaled_zipped_points)] = scaled_zipped_points
    if len(scaled_points) > 1:
        draw_points.colors[:(len(scaled_points)-1)*4] = (1.0, 1.0, 1.0, 1.0)*(len(scaled_points)-1)


#simulation_parameters = {"x": 1., "x'": 1., "Us": 1., "C0": 1., "t0": 0., "tstep": 1e-6, "tend": 1e-3, "R": 0.0, "L": 1.0, "A": 1.0, "omega": 1.0}
simulation_parameters = {"x": 0., "x'": 0., "Us": 0.4, "C0": 18.4e-12, "t0": 0., "tstep": 1e-7, "tend": 1e-4, "R": 100.0, "L": 68e-3, "A": 3.0, "f": 33e3}
output_queue = Queue()
simulate(output_queue)
while True:

    point = output_queue.get()
    if type(point) == tuple:
        (last_t, last_point) = point
        break
    points.append(point)

scaler = { "xscale": 0.0, "yscale": 0.0, "xoffset": 0.0, "yoffset": 0.0, "xmin": 0.0, "ymin": 0.0 }
autoscale()


VBLEN = 2**20
draw_points = pyglet.graphics.vertex_list(VBLEN,
        'v2i',
        ('c4f', (0.0, 0.0, 0.0, 0.0)*VBLEN)
        )

read_scaled_points()

#entry_x = 0
#entry_x_dash = 0
#entry_state = 0

ui_state = { "state": "normal", "number_entry": None, "continue simulating": False, "simulating": False }

label = pyglet.text.Label("", x=10, y=410, anchor_y="top", color=(255, 255, 255, 255), multiline=True, width=100)

@window.event
def on_draw():
    
    window.clear()
    pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
    pyglet.gl.glBlendFunc(pyglet.gl.GL_SRC_ALPHA, pyglet.gl.GL_ONE_MINUS_SRC_ALPHA)
    draw_points.draw(pyglet.gl.GL_LINE_STRIP)

    if ui_state["state"] == "normal":
        label.text = "done."
    elif ui_state["state"] == "combined_entry_x":
        label.text = "x:  "+str(ui_state["number_entry"])
    elif ui_state["state"] == "combined_entry_x'":
        label.text = "x:  "+str(ui_state["new_x"])+"\nx': "+str(ui_state["number_entry"])
    elif ui_state["state"] == "enter parameter":
        label.text = "parameter:"
    elif ui_state["state"] == "enter parameter value":
        label.text = ui_state["parameter"]+": "+str(ui_state["number_entry"])
    label.draw()


@window.event
def on_text(text):

    if ui_state["state"] == "normal":
        if text[0] == " ":
            ui_state["state"] = "combined_entry_x"
            ui_state["number_entry"] = number_entry()
        elif text[0] == "s":
            autoscale()
            read_scaled_points()
        elif text[0] == "\r" or text[0] == "\n":
            ui_state["state"] = "enter parameter"
        elif text[0] == "p":
            data = open("datadump", "w")
            print "start saving data"
            pickle.dump(points, data)
            data.close()
            print "finished saving data"

        elif text[0] == "d":
            if ui_state["simulating"] == False and ui_state["continue simulating"] == False:
                init_progressive_simulate(reset = True, change_parameters = {"x": last_point.x, "x'": last_point.y, "t0": last_t, "tend": simulation_parameters["tstep"]*1000+last_t})


    elif ui_state["state"] == "combined_entry_x":
        if text[0] == "\r" or text[0] == "\n":
            ui_state["state"] = "combined_entry_x'"
            ui_state["new_x"] = ui_state["number_entry"].get_number()
            ui_state["number_entry"] = number_entry()
        else:
            ui_state["number_entry"].keypress(text[0])

    elif ui_state["state"] == "combined_entry_x'":
        if text[0] == "\r" or text[0] == "\n":
            simulation_parameters["x"] = ui_state["new_x"]
            simulation_parameters["x'"] = ui_state["number_entry"].get_number()
            init_progressive_simulate()
            #simulate()
            #scale()
            ui_state["state"] = "normal"
        else:
            ui_state["number_entry"].keypress(text[0])

    elif ui_state["state"] == "enter parameter":
        if text[0] in ["s", "c", "l", "r", "a", "f", "t"]:
            if text[0] == "f":
                ui_state["parameter"] = "f"
            elif text[0] == "s":
                ui_state["parameter"] = "Us"
            elif text[0] == "c":
                ui_state["parameter"] = "C0"
            elif text[0] == "l":
                ui_state["parameter"] = "L"
            elif text[0] == "r":
                ui_state["parameter"] = "R"
            elif text[0] == "a":
                ui_state["parameter"] = "A"
            elif text[0] == "t":
                ui_state["parameter"] = "tstep"

            ui_state["number_entry"] = number_entry()
            ui_state["state"] = "enter parameter value"

    elif ui_state["state"] == "enter parameter value":

        if text[0] == "\r" or text[0] == "\n":
            value = ui_state["number_entry"].get_number()
            simulation_parameters[ui_state["parameter"]] = value

            init_progressive_simulate()
            ui_state["parameter"] = None
            ui_state["state"] = "normal"
        else:
            ui_state["number_entry"].keypress(text[0])

@window.event
def on_key_press(key, modifiers):

    if key == pyglet.window.key.LSHIFT:
        ui_state["continue simulating"] = True

    elif key == pyglet.window.key.BACKSPACE:
        if ui_state["number_entry"]:
            ui_state["number_entry"].keypress(chr(127))

@window.event
def on_key_release(key, modifiers):

    if key == pyglet.window.key.LSHIFT:
        ui_state["continue simulating"] = False

@window.event
def on_mouse_scroll(x, y, scroll_x, scroll_y):

    factor = 1.1
    if scroll_y < 0:
        factor = 1/factor

    scaler["xoffset"] = (((scaler["xoffset"]-scaler["xmin"])*scaler["xscale"]+x)*factor-x)/factor/scaler["xscale"] + scaler["xmin"]
    scaler["yoffset"] = (((scaler["yoffset"]-scaler["ymin"])*scaler["yscale"]+y)*factor-y)/factor/scaler["yscale"] + scaler["ymin"]

    scaler["xscale"] *= factor
    scaler["yscale"] *= factor

    #scaler["xoffset"] = scaler["xoffset"]*factor-x*(1/scaler["xscale"]-factor*factor/scaler["xscale"])*factor*factor*factor*factor
    #scaler["yoffset"] = scaler["yoffset"]*factor-y*(1/scaler["yscale"]-factor*factor/scaler["yscale"])

    scale()
    read_scaled_points()

@window.event
def on_mouse_drag(x, y, dx, dy, buttons, modifiers):

    scaler["xoffset"] += dx/scaler["xscale"]
    scaler["yoffset"] += dy/scaler["yscale"]
    scale()
    read_scaled_points()

@window.event
def test_key(dt):

    if ui_state["continue simulating"] == True and ui_state["simulating"] == False:
        init_progressive_simulate(reset = False, change_parameters = {"x": last_point.x, "x'": last_point.y, "t0": last_t, "tend": simulation_parameters["tstep"]*1000+last_t})


pyglet.clock.schedule_interval_soft(test_key, 1/30.)

pyglet.app.run()
