# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import math

#def add(vec_a, vec_b):
#    return [vec_a[0]+vec_b[0], vec_a[1]+vec_b[1]]
#
#def sub(vec_a, vec_b):
#    return [vec_a[0]-vec_b[0], vec_a[1]-vec_b[1]]
#
#def mul(vec, sca):
#    return [vec[0]*sca, vec[1]*sca]
#
#def abs(vec):
#    return math.sqrt(vec[0]**2+vec[1]**2)
#
#def norm(vec):
#    length = abs(vec)
#    return mul(vec, 1/length)
#
#def vecint(vec):
#    return [int(vec[0]), int(vec[1])]

class vec2:

    def __init__(self, *args):
        """Takes coordinates either as a tuple or separate"""
        self.set(args)

    def set(self, *args):
        """Takes coordinates either as a tuple or separate"""
        if len(args) == 1:
            arg = args[0]
            self.x = float(arg[0])
            self.y = float(arg[1])
        elif len(args) == 2:
            self.x = float(args[0])
            self.y = float(args[1])
        else:
            raise TypeError("vec2.__init__() takes either one or two arguments; "+len(args)+" given.")

    def coords(self):
        return self.x, self.y

    def __add__(self, other):
        if type(other) == type(self):
            return vec2(self.x+other.x, self.y+other.y)
        else:
            raise TypeError

    def __sub__(self, other):
        if type(other) == type(self):
            return vec2(self.x-other.x, self.y-other.y)
        else:
            raise TypeError

    def __mul__(self, other):
        if type(other) == type(self):
            return self.x*other.x+self.y*other.y
        elif type(other) == float or type(other) == int:
            return vec2(self.x*other, self.y*other)

    def __repr__(self):
        return "vec2 ("+str(self.x)+", "+str(self.y)+")"

    def __getitem__(self, i):
        if i == 0:
            return self.x
        elif i == 1:
            return self.y

    def __setitem__(self, i, value):
        if i == 0:
            self.x = value
        elif i == 1:
            self.y = value

    def __abs__(self):
        return math.sqrt(self[0]**2+self[1]**2)

    def norm(self):
        length = abs(self)
        return self*(1/length);
